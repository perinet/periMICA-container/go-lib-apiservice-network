/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://httpserver.io/contact/ when you want license
this software under a commercial license.
*/

package network

import (
	"encoding/json"
	"log"
	"net"
	"net/http"
	"os"
	"strings"
	"gitlab.com/perinet/generic/lib/utils/intHttp"

	"gitlab.com/perinet/generic/lib/httpserver"
	"gitlab.com/perinet/generic/lib/httpserver/periHttp"
	"gitlab.com/perinet/generic/lib/httpserver/rbac"
	"gitlab.com/perinet/periMICA-container/apiservice/node"
	"gitlab.com/perinet/periMICA-container/apiservice/node/nodeErrorStatus"
)

func PathsGet() []httpserver.PathInfo {
	return []httpserver.PathInfo{
		{Url: "/network", Method: httpserver.GET, Role: rbac.USER, Call: Network_Info_Get},
		{Url: "/network/interfaces", Method: httpserver.GET, Role: rbac.ADMIN, Call: Network_Interfaces_Get},
		{Url: "/network/interfaces/{interface}", Method: httpserver.GET, Role: rbac.ADMIN, Call: Network_Interface_Get},
	}
}

const (
	API_VERSION = "23"
)

type interface_object struct {
	Mac_address string   `json:"mac_address"`
	Adresses    []string `json:"addresses"`
}

type network_object struct {
	ApiVersion json.Number                 `json:"api_version"`
	Hostname   string                      `json:"hostname"`
	Interfaces map[string]interface_object `json:"interfaces"`
	ErrorStatus node.NodeErrorStatus       `json:"error_status"`
}

var (
	logger            = *log.Default()
	net_object        network_object
	os_hostname                            = os.Hostname
	currentNodeStatus node.NodeErrorStatus = nodeErrorStatus.NO_ERROR
)

func init() {
	logger.SetPrefix("Network Service: ")
	net_object.Interfaces = make(map[string]interface_object)
	net_object.ApiVersion = API_VERSION
	net_object.ErrorStatus = currentNodeStatus
	refresh_hostname()
	refresh_ip_addresses()
}

func refresh_hostname() (string, error) {
	name, err := os_hostname()
	if err != nil {
		updateNodeStatus(nodeErrorStatus.WARNING, err.Error())
	}
	return name, err
}

func refresh_ip_addresses() (map[string]interface_object, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		updateNodeStatus(nodeErrorStatus.ERROR, err.Error())
		return nil, err
	}
	res := make(map[string]interface_object)
	for _, i := range ifaces {
		addrs, err := i.Addrs()
		if err != nil {
			logger.Print(err)
			continue
		}
		hw_addr := i.HardwareAddr.String()
		var tmp interface_object
		tmp.Mac_address = hw_addr
		add_if := false
		for _, a := range addrs {
			ipnet, ok := a.(*net.IPNet)
			if ok && !ipnet.IP.IsLoopback() {
				tmp.Adresses = append(tmp.Adresses, a.String())
				add_if = true
			}
		}
		if add_if {
			res[i.Name] = tmp
		}
	}
	return res, nil
}

func updateNodeStatus(newNodeStatus node.NodeErrorStatus, reasons ...string) {
	printReasons := func() {
		if len(reasons) > 0 {
			logger.Print(strings.Join(reasons, " "))
		}
	}
	if currentNodeStatus < newNodeStatus {
		logger.Println("Update NodeErrorStatus " + currentNodeStatus.String() + " -> " + newNodeStatus.String())
		printReasons()
		currentNodeStatus = newNodeStatus
		data, _ := json.Marshal(currentNodeStatus)
		intHttp.Put(node.ErrorStatusSet, data, nil)
	} else {
		logger.Println("Update NodeErrorStatus supressed " + newNodeStatus.String())
		printReasons()
	}
}

func Network_Info_Get(p periHttp.PeriHttp) {
	interfaces, err := refresh_ip_addresses()
	if err != nil {
		logger.Print(err)
	} else {
		net_object.Interfaces = interfaces
	}
	hostname, err := refresh_hostname()
	if err != nil {
		logger.Print(err)
	} else {
		net_object.Hostname = hostname
	}
	// update node error_status
	var nodeInfo node.NodeInfo
	data := intHttp.Get(node.NodeInfoGet, nil)
	json.Unmarshal(data, &nodeInfo)
	net_object.ErrorStatus = nodeInfo.ErrorStatus

	binaryJson, err := json.Marshal(net_object)
	if err != nil {
		p.JsonResponse(http.StatusInternalServerError, map[string]any{"error": err.Error()})
		return
	}
	p.JsonResponse(http.StatusOK, binaryJson)
}

func Network_Interfaces_Get(p periHttp.PeriHttp) {
	interfaces, err := refresh_ip_addresses()
	if err != nil {
		logger.Print(err)
	} else {
		net_object.Interfaces = interfaces
	}
	p.JsonResponse(http.StatusOK, net_object.Interfaces)
}

func Network_Interface_Get(p periHttp.PeriHttp) {
	interfaces, err := refresh_ip_addresses()
	if err != nil {
		logger.Print(err)
	} else {
		net_object.Interfaces = interfaces
	}
	interfaceName := p.Vars()["interface"]
	net_if := net_object.Interfaces[interfaceName]
	p.JsonResponse(http.StatusOK, net_if)
}
