# API Service Network

The api service Network is the implementation of the network part from the
[unified api](https://gitlab.com/perinet/unified-api). The api service network is
available in all Perinets Products. 

This implementation does store its resources, e.g. NetworkInfo object or the ProductionInfo object persistently in the file system `/var/lib/apiservice-network/` as json files.

## References

* [openAPI spec for apiservice network] https://gitlab.com/perinet/unified-api/-/blob/main/services/Network_openapi.yaml


# Dual License

This software is by default licensed via the GNU Affero General Public License
version 3. However it is also available with a commercial license on request
(https://perinet.io/contact).