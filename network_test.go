/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package network

import (
	"encoding/json"
	"testing"

	"gitlab.com/perinet/generic/lib/utils/intHttp"
	"gotest.tools/v3/assert"
)

func TestApiVersion(t *testing.T) {
	var net_obj network_object
	data := intHttp.Get(Network_Info_Get, nil)
	json.Unmarshal(data, &net_obj)

	assert.Assert(t, net_obj.ApiVersion == "23")
}

func TestHostname(t *testing.T) {
	const host = "hotte"

	// mock os.Hostname()
	old := os_hostname
	defer func() { os_hostname = old }()
	os_hostname = func() (name string, err error) {
		return host, nil
	}

	var net_obj network_object
	data := intHttp.Get(Network_Info_Get, nil)
	json.Unmarshal(data, &net_obj)

	assert.Assert(t, net_obj.Hostname == host)
}
