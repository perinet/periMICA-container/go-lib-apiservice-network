module gitlab.com/perinet/periMICA-container/apiservice/network

go 1.22

toolchain go1.22.3

require (
	gitlab.com/perinet/generic/lib/httpserver v1.0.1-0.20240916100425-406ce7a80d39
	gitlab.com/perinet/generic/lib/utils v1.0.1-0.20240621122644-d12809863382
	gitlab.com/perinet/periMICA-container/apiservice/node v1.0.4
	gotest.tools/v3 v3.5.1
)

require (
	github.com/alexandrevicenzi/go-sse v1.6.0 // indirect
	github.com/felixge/httpsnoop v1.0.4 // indirect
	github.com/google/go-cmp v0.6.0 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/gorilla/handlers v1.5.2 // indirect
	github.com/gorilla/mux v1.8.1 // indirect
	golang.org/x/exp v0.0.0-20240613232115-7f521ea00fb8 // indirect
)
